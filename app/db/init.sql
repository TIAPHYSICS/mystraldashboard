-- THE FOLLOWING CODE CREATES ALL THE TABLES FOR THE MYSTRAL TOOL

/* 
THE TABLE operatore CONTAINS THE ALL THE INFOS ABOUT THE OPERATORS THAT EXECUTED THE JOB-CALL.
*/

CREATE TABLE operatore (
   token_operatore INTEGER PRIMARY KEY,
   den_nome        VARCHAR(250) NOT NULL,
   den_cognome     VARCHAR(250) NOT NULL,
   sede_operativa  VARCHAR(250) NOT NULL,
   data_timestamp  DATETIME NOT NULL
);

INSERT INTO operatore (token_operatore,den_nome,den_cognome,sede_operativa,data_timestamp)
VALUES(1,'DAVID','BOWIE','LONDRA',datetime('now'));
INSERT INTO operatore (token_operatore,den_nome,den_cognome,sede_operativa,data_timestamp)
VALUES(2,'FREDDIE','MERCURY','ZANZIBAR',datetime('now'));
INSERT INTO operatore (token_operatore,den_nome,den_cognome,sede_operativa,data_timestamp)
VALUES(3,'TILL','LINDEMANN','LIPSIA',datetime('now'));

/* 
THE TABLE cliente CONTAINS THE ALL THE LOGINS OF THE JOB-CALL OPERATORS.
IN OUR CASE, ALL THE LOGIN ARE PRE_LOADED
*/

CREATE TABLE login_operatore (
   token_login_operatore INTEGER PRIMARY KEY,
   token_operatore       INTEGER,
   den_login             VARCHAR(30) NOT NULL,
   den_password          VARCHAR(30) NOT NULL,
   data_timestamp        DATETIME NOT NULL
);

INSERT INTO login_operatore (token_login_operatore,token_operatore,den_login,den_password,data_timestamp)
VALUES(1,1,'LiveAid','1985',datetime('now'));
INSERT INTO login_operatore (token_login_operatore,token_operatore,den_login,den_password,data_timestamp)
VALUES(2,2,'LiveWembley','1986',datetime('now'));
INSERT INTO login_operatore (token_login_operatore,token_operatore,den_login,den_password,data_timestamp)
VALUES(3,3,'LiveBerlin','1998',datetime('now'));

/* 
THE TABLE cliente CONTAINS THE ALL THE INFOS ABOUT CLIENT OF THE JOB-CALL.
*/

CREATE TABLE cliente (
   token_cliente  INTEGER PRIMARY KEY,
   desc_cliente   VARCHAR(250) NOT NULL,
   nota_cliente   TEXT NOT NULL,   
   data_timestamp DATETIME NOT NULL
);

INSERT INTO cliente (token_cliente,desc_cliente,nota_cliente,data_timestamp)
VALUES(1,'MY-FARM','Rappresenta l''azienda my-farm stessa',datetime('now'));
INSERT INTO cliente (token_cliente,desc_cliente,nota_cliente,data_timestamp)
VALUES(2,'EXT-CORP','Rappresenta l''azienda esterna di prova',datetime('now'));

/* 
THE TABLE tipo_categoria_job_call CONTAINS THE ALL THE INFOS ABOUT THE CLASSIFICATION OF THE JOB-CALL.
*/

CREATE TABLE tipo_categoria_job_call (
   den_categoria_job_call    VARCHAR(250) NOT NULL,
   tipo_categoria_job_call   VARCHAR(250) NOT NULL,   
   colore_categoria_job_call VARCHAR(10) NOT NULL
);

INSERT INTO tipo_categoria_job_call (den_categoria_job_call,tipo_categoria_job_call,colore_categoria_job_call)
VALUES('ESTERNA','E','deepskyblue');
INSERT INTO tipo_categoria_job_call (den_categoria_job_call,tipo_categoria_job_call,colore_categoria_job_call)
VALUES('INTERNA','I','gold');

/* 
THE TABLE job_call CONTAINS THE ALL THE INFOS ABOUT THE JOB-CALL.
THE FIELD token_operatore ASSOCIATES THE JOB-CALL TO THE OPERATOR THAT EXECUTED IT
*/

CREATE TABLE job_call (
   token_job_call          INTEGER PRIMARY KEY,
   token_cliente           INTEGER NOT NULL,
   token_operatore         INTEGER NOT NULL,
   desc_job_call           VARCHAR(250) NOT NULL,
   tipo_categoria_job_call VARCHAR(250) NOT NULL,
   data_timestamp          DATETIME NOT NULL
);

INSERT INTO job_call (token_job_call,token_cliente,token_operatore,desc_job_call,tipo_categoria_job_call,data_timestamp)
VALUES(1,1,3,'GROWL MELODIC SINGER POSITION','I',datetime('now'));
INSERT INTO job_call (token_job_call,token_cliente,token_operatore,desc_job_call,tipo_categoria_job_call,data_timestamp)
VALUES(2,1,2,'ROCK GUITARIST POSITION','I',datetime('now'));
INSERT INTO job_call (token_job_call,token_cliente,token_operatore,desc_job_call,tipo_categoria_job_call,data_timestamp)
VALUES(3,2,2,'ROCK DRUMMER POSITION','E',datetime('now'));

/* 
THE TABLE fav_operator_calls ASSOCIATES TO EACH OPERATOR THE FAVOUTITE CALLS
*/

CREATE TABLE fav_operator_calls (
   token_fav_operator_calls INTEGER PRIMARY KEY,
   token_operatore          INTEGER NOT NULL,
   token_job_call           INTEGER PNOT NULL
);

INSERT INTO fav_operator_calls (token_fav_operator_calls,token_operatore,token_job_call)
VALUES(1,3,1);
INSERT INTO fav_operator_calls (token_fav_operator_calls,token_operatore,token_job_call)
VALUES(2,3,3);
INSERT INTO fav_operator_calls (token_fav_operator_calls,token_operatore,token_job_call)
VALUES(3,2,1);
