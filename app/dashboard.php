<html>
    <head>
        <title>myStral</title>
        <link href="./resources/css/dashboard_sty.css" type="text/css" rel="stylesheet">
    </head>
    
    <body>

        <?php 
            session_start();
            include $_SERVER['DOCUMENT_ROOT'].'/resources/php/config.php';
            include $_SERVER['DOCUMENT_ROOT'].'/resources/php/manage_operatore.php';
            include $_SERVER['DOCUMENT_ROOT'].'/resources/php/manage_filters.php';
        ?>
   
        <div class="grid-container">

            <header class="header">
                <?php include $_SERVER['DOCUMENT_ROOT'].'/resources/php/manage_header.php';?>     
            </header>

            <aside class="sidenav">
                <?php include $_SERVER['DOCUMENT_ROOT'].'/resources/php/manage_sidebar.php';?> 
            </aside>

            <main class="main">
                <?php include $_SERVER['DOCUMENT_ROOT'].'/resources/php/manage_main.php';?> 
            </main>

            <footer class="footer">
                <?php include $_SERVER['DOCUMENT_ROOT'].'/resources/php/manage_footer.php';?> 
            </footer>
            
        </div>

    </body>

</html>